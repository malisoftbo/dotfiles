return {
  "xiyaowong/nvim-transparent",
  --enabled = false,
  lazy = false,
  cmd = { "TransparentEnable", "TransparentDisable" },
  init = function()
    vim.g.transparent_enabled = false
  end,
  opts = {   -- Optional, you don't have to run setup.
    groups = { -- table: default groups
      "Normal",
      "NormalNC",
      "Constant",
      "Special",
      "Identifier",
      "Statement",
      "PreProc",
      "Type",
      "Underlined",
      "String",
      "Function",
      "Conditional",
      "Repeat",
      "Operator",
      "Structure",
      "LineNr",
      "NonText",
      "SignColumn",
      --"CursorLine",
      --"CursorLineNr",
      --"StatusLine",
      "StatusLineNC",
      "EndOfBuffer",
    },
    extra_groups = {}, -- table: additional groups that should be cleared
    exclude_groups = {}, -- table: groups you don't want to clear
  },
}
