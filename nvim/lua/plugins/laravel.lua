return {
  "adalessa/laravel.nvim",
  dependencies = {
    "nvim-telescope/telescope.nvim",
    "tpope/vim-dotenv",
    "MunifTanjim/nui.nvim",
    "nvimtools/none-ls.nvim",
  },
  cmd = { "Sail", "Artisan", "Composer", "Npm", "Yarn", "Laravel" },
  keys = {
    { "<leader>la", ":Laravel artisan<cr>" },
    { "<leader>lr", ":Laravel routes<cr>" },
    { "<leader>lm", ":Laravel related<cr>" },
  },
  event = { "BufReadPost" },
  config = function()
    require("laravel").setup({
      ui = {
        default = "split",
        nui_opts = {
          split = {
            enter = true,
            relative = "editor",
            position = "right",
            size = "33%",
            buf_options = {},
            win_options = {
              number = false,
              relativenumber = false,
            },
          },
          popup = {
            enter = true,
            focusable = true,
            relative = "editor",
            border = {
              style = "rounded",
            },
            position = {
              row = "10%",
              col = "10%",
            },
            size = {
              width = "28%",
              height = "45%",
            },
            buf_options = {},
            win_options = {
              number = false,
              relativenumber = false,
            },
          },
        },
      },
    })
  end,
}
