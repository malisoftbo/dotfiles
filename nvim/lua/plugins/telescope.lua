return {
  {
    "nvim-telescope/telescope.nvim",
    dependencies = {
      { "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
      "nvim-telescope/telescope-file-browser.nvim",
      "nvim-telescope/telescope-project.nvim",
      "ahmedkhalf/project.nvim",
      { "cljoly/telescope-repo.nvim",               version = false },
      {
        "stevearc/aerial.nvim",
        opts = {
          keymaps = {
            ["?"] = "actions.show_help",
            ["g?"] = "actions.show_help",
            ["<CR>"] = "actions.jump",
            ["<2-LeftMouse>"] = "actions.jump",
            ["<C-v>"] = "actions.jump_vsplit",
            ["<C-s>"] = "actions.jump_split",
            ["p"] = "actions.scroll",
            ["<C-j>"] = "actions.down_and_scroll",
            ["<C-k>"] = "actions.up_and_scroll",
            ["{"] = "actions.prev",
            ["}"] = "actions.next",
            ["[["] = "actions.prev_up",
            ["]]"] = "actions.next_up",
            ["q"] = "actions.close",
            ["o"] = "actions.tree_toggle",
            ["za"] = "actions.tree_toggle",
            ["O"] = "actions.tree_toggle_recursive",
            ["zA"] = "actions.tree_toggle_recursive",
            ["l"] = "actions.tree_open",
            ["zo"] = "actions.tree_open",
            ["L"] = "actions.tree_open_recursive",
            ["zO"] = "actions.tree_open_recursive",
            ["h"] = "actions.tree_close",
            ["zc"] = "actions.tree_close",
            ["H"] = "actions.tree_close_recursive",
            ["zC"] = "actions.tree_close_recursive",
            ["zr"] = "actions.tree_increase_fold_level",
            ["zR"] = "actions.tree_open_all",
            ["zm"] = "actions.tree_decrease_fold_level",
            ["zM"] = "actions.tree_close_all",
            ["zx"] = "actions.tree_sync_folds",
            ["zX"] = "actions.tree_sync_folds",
          },
        },
      },
      "nvim-lua/plenary.nvim",
      "nvim-telescope/telescope-media-files.nvim",
    },
    cmd = "Telescope",
    keys = {
      { "<leader>fo", "<cmd>Telescope oldfiles<cr>",     desc = "Recent" },
      { "<leader>fb", "<cmd>Telescope buffers<cr>",      desc = "Buffers" },
      { "<leader>fr", "<cmd>Telescope file_browser<cr>", desc = "Browser" },
      { "<leader>ff", "<cmd>Telescope find_files<cr>",   desc = "Search Files" },
      { "<leader>rl", "<cmd>Telescope repo list<cr>",    desc = "Search repo" },
      { "<leader>ht", "<cmd>Telescope help_tags<cr>",    desc = "Search Help about tags" },
      { "<leader>fp", "<cmd>Telescope media_files<cr>",  desc = "Search and Preview Images" },
      {
        "<leader>pp",
        function()
          require("telescope").extensions.project.project({ display_type = "minimal" })
        end,
        desc = "List",
      },
      { "<leader>fg", "<cmd>Telescope live_grep<cr>", desc = "Workspace" },
      { "<leader>fc", "<cmd>Telescope commands<cr>",  desc = "Commands" },
      {
        "<leader>sb",
        function()
          require("telescope.builtin").current_buffer_fuzzy_find()
        end,
        desc = "Buffer",
      },
      { "<leader>st", "<cmd>Telescope aerial<cr>", desc = "Outline" },
    },
    config = function(_, _)
      local telescope = require("telescope")
      local icons = require("merlo.icons")
      local actions = require("telescope.actions")
      local actions_layout = require("telescope.actions.layout")
      local mappings = {
        i = {
          ["<C-j>"] = actions.move_selection_next,
          ["<C-k>"] = actions.move_selection_previous,
          ["<C-n>"] = actions.cycle_history_next,
          ["<C-p>"] = actions.cycle_history_prev,
          ["?"] = actions_layout.toggle_preview,
          --open all selected
          ["<C-x>"] = actions.send_to_qflist + actions.open_qflist,
          ["<C-q>"] = actions.send_selected_to_qflist + actions.open_qflist,
          ["<C-s>"] = actions.select_horizontal,
          ["<C-v>"] = actions.select_vertical,
          ["<C-t>"] = actions.select_tab,
          --["<C-u>"] = actions.select_all,
          ["<C-c>"] = actions.close,
          ["<C-a>"] = actions.toggle_all,
          ["<C-e>"] = actions.delete_buffer,
          ["<C-o>"] = actions.smart_send_to_qflist + actions.open_qflist,
        },
      }

      local opts = {
        file_ignore_patters = {
          "node_modules",
          ".git",
          "__pycache__",
          "^public/vendor/",
        },
        defaults = {
          prompt_prefix = icons.ui.Telescope .. " ",
          selection_caret = icons.ui.Forward .. " ",
          mappings = mappings,
          border = {},
          borderchars = { "─", "│", "─", "│", "╭", "╮", "╯", "╰" },
          color_devicons = true,
        },
        pickers = {
          find_files = {
            theme = "dropdown",
            previewer = false,
            hidden = true,
            find_command = { "rg", "--files", "--hidden", "-g", "!.git" },
            file_ignore_patterns = {
              "node_modules"
            },
          },
          git_files = {
            theme = "dropdown",
            previewer = false,
          },
          buffers = {
            theme = "dropdown",
            previewer = false,
          },
        },
        extensions = {
          file_browser = {
            theme = "dropdown",
            previewer = true,
            hijack_netrw = true,
            mappings = mappings,
          },
          project = {
            hidden_files = false,
            theme = "dropdown",
          },
          repo = {
            list = {
              fd_opts = {
                "--no-ignore-vcs",
              },
              search_dirs = {
                "~/Projects",
              },
            },
          },
          --live_grep ignore node_modules folder
          live_grep = {
            path_display = { "shorten" },
            layout_config = {
              preview_width = 0.65,
            },
            grep_open_files = true,
            grep_open_files_with_term = true,
            --[[ search_dirs = {
              "~/my_projects",
            }, ]]
            search = {
              "rg",
              "--hidden",
              "--no-heading",
              "--with-filename",
              "--line-number",
              "--column",
              "--smart-case",
              "--ignore",
              "--glob",
              "!.git",
              "--glob",
              "!node_modules",
              "!public",
            },
          },
          media_files = {
            filetypes = { "png", "jpg", "pdf", "jpeg" },
            find_cmd = "rg",
          },
        },
      }
      telescope.setup(opts)
      telescope.load_extension("fzf")
      telescope.load_extension("file_browser")
      telescope.load_extension("project")
      telescope.load_extension("projects")
      telescope.load_extension("aerial")
      telescope.load_extension("repo")
      telescope.load_extension("media_files")
    end,
  },
  {
    "ahmedkhalf/project.nvim",
    config = function()
      require("project_nvim").setup({
        detection_methods = { "pattern", "lsp" },
        patterns = { ".git" },
        ignore_lsp = { "null-ls" },
        --path = "%:p:h"
      })
    end,
  },
}
