return {
  {
    "vhyrro/luarocks.nvim",
    priority = 1000,
    config = true,
    opts = {
      rocks = { "lua-curl", "nvim-nio", "mimetypes", "xml2lua" }
    }
  },
  {
    "rest-nvim/rest.nvim",
    dependencies = { "luarocks.nvim" },
    ft = "http",
    keys = {
      {
        "<leader>rn",
        "<cmd>Rest run<cr>",
        "Run request under the cursor",
      },
      {
        "<leader>rl",
        "<cmd>Rest run last<cr>",
        "Re-run latest request",
      },
    },
    config = function()
      require("rest-nvim").setup()
      require("telescope").load_extension("rest")
    end,
  },
}
