--for focus in the buffer that you are located
return {
  "miversen33/sunglasses.nvim",
  opts = {
    filter_percent=.3,
    filter_type = "SHADE",
  },
  event = "UIEnter",
}
