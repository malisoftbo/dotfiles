-- tailwind-tools.lua
return {
  {
    "js-everts/cmp-tailwind-colors",
    ft = { "typescriptreact", "javascriptreact", "vue" },
    dependencies = {
      "hrsh7th/nvim-cmp",
    },
    opts = {
      enable_alpha = true, -- requires pumblend > 0.
      format = function(itemColor)
        return {
          fg = itemColor,
          bg = itemColor, -- or nil if you dont want a background color
          text = "  ", -- or use an icon
        }
      end,
    },
    config = function(_, opts)
      local icons = require("merlo.icons")
      local source_names = {
        nvim_lsp = "(LSP)",
        luasnip = "(Snippet)",
        buffer = "(Buffer)",
        Codeium = "(Codeium)",
      }
      local duplicates = {
        buffer = 1,
        path = 1,
        nvim_lsp = 0,
        luasnip = 1,
      }
      require("cmp-tailwind-colors").setup(opts)
      require("cmp").setup({
        formatting = {
          format = function(entry, item)
            local max_width = 80
            local duplicates_default = 0
            item.menu = source_names[entry.source.name]
            if item.kind == "Color" then
              item = require("cmp-tailwind-colors").format(entry, item)
              if item.kind ~= "Color" then --when it's not a color
                item.menu = source_names[entry.source.name]
                return item
              end
            else
              item.kind = icons.kind[item.kind]
              if max_width ~= 0 and #item.abbr > max_width then
                item.abbr = string.sub(item.abbr, 1, max_width - 1) .. icons.ui.Ellipsis
              end
            end
            item.menu = source_names[entry.source.name]
            item.dup = duplicates[entry.source.name] or duplicates_default

            return item
          end,
        },
      })
    end,
  },
  {
    "luckasRanarison/tailwind-tools.nvim",
    event = "VeryLazy",
    dependencies = { "nvim-treesitter/nvim-treesitter" },
    opts = {} -- your configuration
  },
  {
    "MaximilianLloyd/tw-values.nvim",
    --ft = { "typescriptreact", "javascriptreact", "vue" },
    keys = {
      { "<leader>sv", "<cmd>TWValues<cr>", desc = "Show tailwind CSS values" },
    },
    opts = {
      border = "rounded",       -- Valid window border style,
      show_unknown_classes = true, -- Shows the unknown classes popup
      focus_preview = true,     -- Sets the preview as the current window
      copy_register = "",       -- The register to copy values to,
      keymaps = {
        copy = "<C-y>",         -- Normal mode keymap to copy the CSS values between {}
      },
    },
  },
}
