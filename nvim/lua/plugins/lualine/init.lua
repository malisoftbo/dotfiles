return {
  {
    "nvim-lualine/lualine.nvim",
    dependencies = {
      "meuter/lualine-so-fancy.nvim",
    },
    event = "VeryLazy",

    config = function()
      local components = require("plugins.lualine.components")
      local icons = require("merlo.icons")
      require("lualine").setup({
        options = {
          icons_enabled = true,
          theme = "auto",
          component_separators = { left = "", right = "" },
          section_separators = { left = "", right = "" },
          disabled_filetypes = {
            statusline = {},
            winbar = {},
          },
          always_divide_middle = true,
          globalstatus = true,
        },
        sections = {
          lualine_a = { components.vim, "mode" },
          lualine_b = { { "branch", icon = "" }, "db_ui#statusline" },
          lualine_c = {
            components.diff,
            {
              "filename",
              { "fancy_cwd", substitute_home = true },
              file_status = true,
              path = 1,
              symbols = {
                modified = icons.ui.Pencil,
                readonly = icons.ui.Lock,
                unnamed = icons.ui.File,
                newfile = icons.ui.NewFile,
              },
            },
            { "fancy_diagnostics" },
            --components.lsp_client,
          },
          lualine_x = {
            --"aerial",
            --components.spaces,
            components.lsp_client,
            "encoding",
            "fileformat",
            "filetype",
            "progress",
            require("recorder").recordingStatus,
            require("recorder").displaySlots,
          },
          lualine_y = {},
          lualine_z = { "location" },
        },
        extensions = { "toggleterm", "quickfix" },
      })
    end,
  },
}
