--to see information about regular expresions regex
return {
  "tomiis4/hypersonic.nvim",
  very_lazy = true,
  cmd = "Hypersonic",
  opts = {
    ---@type 'none'|'single'|'double'|'rounded'|'solid'|'shadow'|table
    border = 'rounded',
    ---@type number 0-100
    winblend = 0,
    ---@type boolean
    add_padding = true,
    ---@type string
    hl_group = 'Keyword',
    ---@type string
    wrapping = '"',
    ---@type boolean
    enable_cmdline = true
  },
}
