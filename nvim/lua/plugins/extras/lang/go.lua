return {
  "ray-x/go.nvim",
  enabled = true,
  dependencies = {  -- optional packages
    "ray-x/guihua.lua",
    "neovim/nvim-lspconfig",
    { "nvim-treesitter/nvim-treesitter", build = ":TSUpdate", version = false },
  },
  config = function()
    require("go").setup({
      diagnostic = {
        virtual_text = false,
      }
    })
  end,
  --event = {"CmdlineEnter"},
  ft = {"go", 'gomod'},
  branch = 'master',
  build = ':lua require("go.install").update_all_sync()' -- if you need to install/update all binaries
}
