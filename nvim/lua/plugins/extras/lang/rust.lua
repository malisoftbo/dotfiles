return {
  {
    "nvim-treesitter/nvim-treesitter",
    opts = function(_, opts)
      vim.list_extend(opts.ensure_installed, { "rust" })
    end,
  },
  {
    "neovim/nvim-lspconfig",
    dependencies = {
      {
        "mrcjkb/rustaceanvim",
        version = "^5", -- Recommended
        --lazy = false, -- This plugin is already lazy
        lazy = true,
        dependencies = {
          "mfussenegger/nvim-dap",
        },
      },
    },
    opts = {
      setup = {
        rust_analyzer = function(_, _)
          local lsp_utils = require("plugins.lsp.utils")
          lsp_utils.on_attach(function(_, buffer)
            vim.keymap.set("n", "<leader>ra", function()
              vim.cmd.RustLsp("codeAction") -- supports rust-analyzer's grouping
              -- or vim.lsp.buf.codeAction() if you don't want grouping.
            end, { silent = true, buffer = buffer, noremap = true, desc = "Runnables" })
            vim.keymap.set("n", "<leader>rd", function()
              vim.cmd.RustLsp("debuggables")
            end, { silent = true, buffer = buffer, noremap = true, desc = "Runnables" })
          end)
          return true
        end,
      },
    },
  },
  {
    "saecki/crates.nvim",
    tag = "stable",
    event = "BufRead",
    ft = "rust",
    config = function()
      require("crates").setup()
    end,
  },
}
