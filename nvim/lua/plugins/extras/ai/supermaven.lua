return {
  enabled = true,
  "supermaven-inc/supermaven-nvim",
  event = "BufReadPost",
  config = function()
    require("supermaven-nvim").setup({
      keymaps = {
        accept_suggestion = "<A-l>",
        clear_suggestion = "<A-x>",
        accept_word = "<A-;>",
      },
      ignore_filetypes = { cpp = true },
      color = {
        suggestion_color = "#ffffff",
        cterm = 244,
      },
      disable_inline_completion = false, -- disables inline completion for use with cmp
      disable_keymaps = false,        -- disables built in keymaps for more manual control
    })
  end,
}
