  --[[ local censor_extmark_opts = function(_, match, _)
    local mask = string.rep('x', vim.fn.strchars(match))
    return {
      virt_text = { { mask, 'Comment' } }, virt_text_pos = 'overlay',
      priority = 200, right_gravity = true,
    }
  end ]]
return {
  {
    'echasnovski/mini.hipatterns',
    version = false,
    event = 'VeryLazy',
    opts = {
      highlighters = {
        returns = {
          pattern = '@return',
          group = 'Special',
        },
        params  = {
          pattern = '@param',
          group = 'Special',
        },
        vars  = {
          pattern = '@var',
          group = 'Special',
          --extmark = censor_extmark_opts,
        },
      },
    },
    config = function(_, opts)
      require('mini.hipatterns').setup(opts)
    end

  }
}
