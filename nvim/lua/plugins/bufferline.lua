return {
  "akinsho/nvim-bufferline.lua",
  event = "VeryLazy",
  keys = {
    { "<leader>h",  "<Cmd>BufferLineMovePrev<CR>" },
    { "<leader>j",  "<Cmd>BufferLineCyclePrev<CR>" },
    { "<leader>k",  "<Cmd>BufferLineCycleNext<CR>" },
    { "<leader>l",  "<Cmd>BufferLineMoveNext<CR>" },
    { "<leader>l",  "<Cmd>BufferLineMoveNext<CR>" },
    { "<leader>p",  "<Cmd>BufferLineTogglePin<CR>" },
    { "<leader>pk", "<Cmd>BufferLinePick<CR>" },
    { "<leader>e",  "<Cmd>bdelete<CR>" },
  },
  config = function(_, _)
    local bufferline = require("bufferline")
    bufferline.setup({
      options = {
        --mode = "buffers",                           -- tabs or buffers
        mode = "pill",                              -- tabs or buffers
        style_preset = bufferline.style_preset.default, -- or bufferline.style_preset.minimal,
        themable = true,                            -- allows highlight groups to be overriden i.e. sets highlights as default
        diagnostics = "nvim_lsp",
        max_name_length = 18,
        max_prefix_length = 14,
        tab_size = 18,
        numbers = "buffer_id",
        indicator = {
          icon = "",
          style = "icon",
        },
        offsets = {
          {
            filetype = "aerial",
            text = "OUTLINE EXPLORER",
            highlight = "Title",
            separator = false,
          },
        },
        always_show_bufferline = true,
        color_icons = true,
        enforce_regular_tabs = false,
        show_buffer_icons = true, -- disable filetype icons for buffers
        show_buffer_close_icons = false,
        show_close_icon = false,
        show_tab_indicators = true,
        show_duplicate_prefix = false, -- whether to show duplicate buffer prefix
        separator_style = { "", "." },

        diagnostics_indicator = nil,
      },
      highlights = {
        buffer_selected = {
          bg = "#ff9e64",
          fg = "#000000",
          bold = true,
        },
        separator = {
          fg = "#ff9e64",
        },
        modified_selected = {
          fg = "#F07800",
          bg = "#ff9e64",
          bold = true,
        },
        numbers_selected = {
          fg = "#000000",
          bg = "#ff9e64",
          bold = true,
        },
        warning_selected = {
          fg = "#aaff00",
          bg = "#ff9e64",
          bold = true,
          italic = true,
        },
        error_selected = {
          fg = "#ff0000",
          bg = "#ff9e64",
          --sp = '#ff0000',
          bold = true,
          italic = true,
        },
        hint_selected = {
          fg = "#002fa0",
          bg = "#ff9e64",
          --sp = '#002fa0',
          bold = true,
          italic = true,
        },

        indicator_selected = {
          fg = "#000000",
          bg = "#ff9e64",
          sp = "#ff9e64",
          underline = true,
        },
        indicator_visible = {
          fg = "#000000",
          bg = "#ff9e64",
        },
      },
    })

    vim.cmd("hi BufferlineIndicatorSelected guifg=#ff9e64")
    --vim.cmd("hi BufferlineIndicatorInactive guifg=#ff9e64")
  end,
}
