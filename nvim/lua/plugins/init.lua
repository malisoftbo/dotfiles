return {
  "nvim-lua/plenary.nvim",
  "MunifTanjim/nui.nvim",
  {
    "kyazdani42/nvim-web-devicons",
    dependencies = { "DaikyXendo/nvim-material-icon" },
    config = function()
      require("nvim-web-devicons").setup({
        override = require("nvim-material-icon").get_icons(),
      })
    end,
  },
  {
    "ziontee113/icon-picker.nvim",
    keys = {
      { "<Leader><Leader>i", "<cmd>IconPickerNormal nerd_font<cr>", mode = "n" },
      { "<Leader><Leader>y", "<cmd>IconPickerYank nerd_font<cr>",   mode = "n" },
      { "<C-o>",             "<cmd>IconPickerInsert nerd_font<cr>", mode = "i" },
    },
    opts = { disable_legacy_commands = true },
  },
  {
    "jwalton512/vim-blade",
    ft = "blade",
    init = function()
      vim.g.blade_suppress_map_keys = 1
    end,
    config = function()
      vim.g.blade_shorten_path = 1
    end,
  },
  { "tpope/vim-repeat",  event = "VeryLazy" },
  { "nacro90/numb.nvim", event = "BufReadPre", config = true },
  {
    "lukas-reineke/indent-blankline.nvim",
    event = "VeryLazy",
    --event = { "BufReadPost", "BufNewFile" },
    main = "ibl",
    config = function(_, _)
      local hooks = require("ibl.hooks")
      local highlight = {
        "RainbowRed",
        "RainbowYellow",
        "RainbowBlue",
        "RainbowOrange",
        "RainbowGreen",
        "RainbowViolet",
        "RainbowCyan",
      }

      local highlightspaces = {
          "CursorColumn",
          "Whitespace",
      }
      -- create the highlight groups in the highlight setup hook, so they are reset
      -- every time the colorscheme changes
      hooks.register(hooks.type.HIGHLIGHT_SETUP, function()
        vim.api.nvim_set_hl(0, "RainbowRed", { fg = "#E06C75" })
        vim.api.nvim_set_hl(0, "RainbowYellow", { fg = "#E5C07B" })
        vim.api.nvim_set_hl(0, "RainbowBlue", { fg = "#61AFEF" })
        vim.api.nvim_set_hl(0, "RainbowOrange", { fg = "#D19A66" })
        vim.api.nvim_set_hl(0, "RainbowGreen", { fg = "#98C379" })
        vim.api.nvim_set_hl(0, "RainbowViolet", { fg = "#C678DD" })
        vim.api.nvim_set_hl(0, "RainbowCyan", { fg = "#56B6C2" })
      end)

      vim.g.rainbow_delimiters = { highlight = highlight }
      require("ibl").setup({
        whitespace = {
            highlight = highlightspaces,
            remove_blankline_trail = false,
        },
        scope = { highlight = highlight },
      })

      hooks.register(hooks.type.SCOPE_HIGHLIGHT, hooks.builtin.scope_highlight_from_extmark)
    end,
  },
  {
    "stevearc/dressing.nvim",
    event = "VeryLazy",
    opts = {
      input = { relative = "editor" },
      select = {
        backend = { "telescope", "fzf", "builtin" },
      },
    },
  },
  {
    "rcarriga/nvim-notify",
    event = "User",
    opts = {
      background_colour = "#000000",
      fps = 30,
      icons = {
        DEBUG = "",
        ERROR = "",
        INFO = "",
        TRACE = "✎",
        WARN = "",
      },
      level = 2,
      minimum_width = 50,
      render = "default",
      stages = "fade_in_slide_out",
      time_formats = {
        notification = "%T",
        notification_history = "%FT%T",
      },
      timeout = 1000,
      top_down = true,
    },
    config = function(_, opts)
      local notify = require("notify")
      notify.setup(opts)
      vim.notify = notify.notify
    end,
  },
  {
    "numToStr/Comment.nvim",
    dependencies = { "JoosepAlviste/nvim-ts-context-commentstring" },
    keys = { { "gcc" }, { "gbc" }, { "gc", mode = "v" }, { "gb", mode = "v" } },
    config = function(_, _)
      local opts = {
        ignore = "^$",
        pre_hook = require("ts_context_commentstring.integrations.comment_nvim").create_pre_hook(),
      }
      require("Comment").setup(opts)
    end,
  },
  {
    "smoka7/hop.nvim",
    version = "*",
    cmd = "HopWord",
    keys = {
      { "<leader>jp", "<Cmd>HopWord<CR>",     mode = { "n" } },
      { "<leader>jo", "<Cmd>HopAnywhere<CR>", mode = { "n" } },
      { "<leader>jn", "<Cmd>hopNodes<CR>",    mode = { "n" } },
    },
    --branch = "v2",
    opts = { keys = "etovxqpdygfblzhckisuran" },
  },
  -- session management
  {
    "folke/persistence.nvim",
    enabled = true,
    event = "BufReadPre",
    opts = { options = { "buffers", "curdir", "tabpages", "winsize", "help" } },
    -- stylua: ignore
    keys = {
      { "<leader>qs", function() require("persistence").load() end,                desc = "Restore Session" },
      { "<leader>ql", function() require("persistence").load({ last = true }) end, desc = "Restore Last Session" },
      { "<leader>qd", function() require("persistence").stop() end,                desc = "Don't Save Current Session" },
    },
  },
  { "tpope/vim-surround", event = "BufReadPre" },
  {
    "folke/persistence.nvim",
    event = "BufReadPre",
    opts = { options = { "buffers", "curdir", "tabpages", "winsize", "help" } },
    -- stylua: ignore
    keys = {
      { "<leader>qs", function() require("persistence").load() end,                desc = "Restore Session" },
      { "<leader>ql", function() require("persistence").load({ last = true }) end, desc = "Restore Last Session" },
      { "<leader>qd", function() require("persistence").stop() end,                desc = "Don't Save Current Session" },
    },
  },
  {
    "petertriho/nvim-scrollbar",
    event = "BufReadPre",
    config = true,
  },
  {
    "lewis6991/satellite.nvim",
    enabled = false,
    event = "BufReadPre",
    opts = {
      gitsigns = {
        enable = true,
        signs = { -- can only be a single character (multibyte is okay)
          add = "│",
          change = "│",
          delete = "-",
        },
      },
    },
  },
  {
    "kevinhwang91/nvim-ufo",
    dependencies = { "kevinhwang91/promise-async" },
    event = "BufReadPost",
    config = function()
      require("ufo").setup({
        open_fold_hl_timeout = 150,
        close_fold_kinds_for_ft = { "imports", "comment" },
        preview = {
          win_config = {
            border = { "╭", "─", "╮", "│", "╯", "─", "╰", "│" },
            winhighlight = "Normal:Folded",
            winblend = 0,
          },
          mappings = {
            scrollU = "<C-u>",
            scrollD = "<C-d>",
            jumpTop = "[",
            jumpBot = "]",
          },
        },
        provider_selector = function(_, _, _)
          --return { "treesitter", "indent" }
          return ""
        end,
      })
      vim.keymap.set("n", "zR", require("ufo").openAllFolds)
      vim.keymap.set("n", "zM", require("ufo").closeAllFolds)
      vim.keymap.set("n", "zr", require("ufo").openFoldsExceptKinds)
      vim.keymap.set("n", "zm", require("ufo").closeFoldsWith) -- closeAllFolds == closeFoldsWith(0)
      vim.keymap.set("n", "zk", function()
        require("ufo").peekFoldedLinesUnderCursor()
      end, { silent = true, desc = "Fold" })
    end,
  },
  {
    "echasnovski/mini.map",
    opts = {},
    keys = {
      {
        "<leader>mm",
        function()
          require("mini.map").toggle({})
        end,
        desc = "Toggle Minimap",
      },
    },
    config = function(_, opts)
      require("mini.map").setup(opts)
    end,
  },
  {
    lazy = false,
    "simnalamburt/vim-mundo",
    config = function(_, _)
      vim.g.mundo_right = 1
      vim.g.mundo_preview_height = 40
      vim.g.mundo_width = 60
    end,
  },
  {
    "tamton-aquib/keys.nvim",
    cmd = "KeysToggle",
  },
  {
    "ptdewey/yankbank-nvim",
    event = "VeryLazy",
    config = function()
      require("yankbank").setup({
        max_entries = 12,
        sep = "",
        keymaps = {
          navigation_next = "j",
          navigation_prev = "k",
        },
        num_behavior = "prefix",
      })
      -- map to '<leader>y'
      vim.keymap.set("n", "<leader>y", "<cmd>YankBank<CR>", { noremap = true })
    end,
  },
  {
    'dwrdx/mywords.nvim',
    keys = {
      {
        '<leader>m', function() require('mywords').hl_toggle() end, desc = 'Highlight Word'},
      {
        '<leader>c', function() require('mywords').uhl_all() end, desc = 'Clear Highlight',

      },

    }
  }
}
