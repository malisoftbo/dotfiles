return {
	"chrisgrieser/nvim-early-retirement",
  enabled = false,
  opts = {
    minimumBufferNum = 4,
    retirementAgeMins = 5,
  },
	event = "VeryLazy",
}
